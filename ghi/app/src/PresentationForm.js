import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            companyName: '',
            title: '',
            synopsis: '',
            conferences: [],
        };

        this.handleChangeConference = this.handleChangeConference.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeCompany = this.handleChangeCompany.bind(this);
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeSynopsis = this.handleChangeSynopsis.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log(data)
        data.presenter_name = data.name;
        data.company_name = data.companyName;
        data.presenter_email = data.email;
        delete data.conferences;
        delete data.name;
        delete data.companyName;
        delete data.email;

        const presentationUrl = `http://localhost:8000${data.conference}presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            const cleared = {
                name: '',
                email: '',
                companyName: '',
                title: '',
                synopsis: '',
                conference: '',
            };
            this.setState(cleared);
        }
    }

    handleChangeConference(event) {
        const value = event.target.value;
        this.setState({ conference: value });
    }
    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    handleChangeEmail(event) {
        const value = event.target.value;
        this.setState({ email: value });
    }
    handleChangeCompany(event) {
        const value = event.target.value;
        this.setState({ companyName: value });
    }
    handleChangeTitle(event) {
        const value = event.target.value;
        this.setState({ title: value });
    }
    handleChangeSynopsis(event) {
        const value = event.target.value;
        this.setState({ synopsis: value });
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({conferences: data.conferences});
        }
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input value={this.state.name} onChange={this.handleChangeName} placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                            <label htmlFor="presenter_name">Presenter Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.email} onChange={this.handleChangeEmail} placeholder="Presenter Email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                            <label htmlFor="presenter_email">Presenter Email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.companyName} onChange={this.handleChangeCompany} placeholder="Company Name" type="text" name="company_name" id="company_name" className="form-control"/>
                            <label htmlFor="company_name">Company Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.title} onChange={this.handleChangeTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis" className="form-label">Synopsis</label>
                            <textarea value={this.state.synopsis} onChange={this.handleChangeSynopsis} className="form-control" name="synopsis" id="synopsis" rows="3"></textarea>
                        </div>
                        <div className="mb-3">
                            <select value={this.state.conference} onChange={this.handleChangeConference} required id="conference" name="conference" className="form-select">
                            <option value="">Choose a conference</option>
                            {this.state.conferences.map(conference => {
                                return (
                                    <option key={conference.href} value={conference.href}>
                                        {conference.name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
        );
    }
}
export default PresentationForm;
